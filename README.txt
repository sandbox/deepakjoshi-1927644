CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Frequently Asked Questions (FAQ)
 * Known Issues
 * How Can You Contribute?


INTRODUCTION
------------

Current Maintainer: Deepak Joshi <http://drupal.org/user/531640>
Project Page: http://drupal.org/project/admin_links

This is a very simple module that adds the content local tabs (Edit, Delete,
Revisions, etc.) as links to teasers/lists of nodes. It also makes the Delete
link show up as a tab in the individual node view just like the Edit and View
tabs (Drupal 7 only).

This module also provides support for the Universal Edit Button, a green pencil
icon in the address bar that indicates a web page is editable. It is similar to
the orange "broadcast" RSS icon that indicates there is an RSS feed available.
For more information and to download the Universal Edit Button browser
extension, visit http://universaleditbutton.org/.


INSTALLATION
------------

See https://drupal.org/documentation/install/modules-themes for instructions on
how to install or update Drupal modules.


CONFIGURATION
-------------

Once Admin links is installed and enabled, you can configure the admin links
at admin/config/content/admin-links.
And don't forget to check the module's permissions.


FREQUENTLY ASKED QUESTIONS
--------------------------

There are no frequently asked questions at this time.


KNOWN ISSUES
------------

There are no known issues at this time.

To report new bug reports, feature requests, and support requests, visit
http://drupal.org/project/issues/admin_links_d7.


HOW CAN YOU CONTRIBUTE?
---------------------

- Write a review for this module at drupalmodules.com.
  http://drupalmodules.com/module/admin-links

- Help translate this module.
  http://localize.drupal.org/translate/projects/admin_links_d7

- Report any bugs, feature requests, etc. in the issue tracker.
  http://drupal.org/project/issues/admin_links_d7
