<?php
/**
 * @file
 * Admin page callbacks for the admin_links_d7 module.
 */

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function admin_links_d7_settings_form() {
  // Rebuild menu cache "after form has been submitted" if needed
  // @see _admin_links_d7_system_settings_form_submit()
  if (variable_get('admin_links_d7_menu_rebuild_queued', 0) == 1) {
    menu_rebuild();
    drupal_set_message('Menus rebuild.', 'status');
    variable_set('admin_links_d7_menu_rebuild_queued', 0);
  }

  // form
  $form['fieldset_global'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global settings (teaser and node)'),
    '#collapsible' => TRUE,
  );
  $form['fieldset_global']['admin_links_d7_link_new'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add a link to create a new content of the same type (for example, <em>New Story</em>).'),
    '#default_value' => variable_get('admin_links_d7_link_new', 0),
  );
  $form['fieldset_teaser'] = array(
    '#type' => 'fieldset',
    '#title' => t('Teaser settings'),
    '#collapsible' => TRUE,
  );
  $form['fieldset_teaser']['admin_links_d7_link_tasks'] = array(
    '#type' => 'checkbox',
    '#title' => t('Move node local tasks into the teaser links.'),
    '#default_value' => variable_get('admin_links_d7_link_tasks', 1),
  );
  $form['fieldset_teaser']['admin_links_d7_exclude'] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude the following content local tasks or links from being added to the content teaser links'),
    '#description' => t('One path per line. You can use <em>*</em> as a wildcard.'),
    '#default_value' => variable_get('admin_links_d7_exclude', implode("\n", array('node/*/view', 'node/*/devel/*'))),
    '#wysiwyg' => FALSE,
  );
  $form['fieldset_node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node settings'),
    '#collapsible' => TRUE,
  );
  $form['fieldset_node']['admin_links_d7_node_delete_tab_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable \'Delete\' tab for nodes.'),
    '#default_value' => variable_get('admin_links_d7_node_delete_tab_active', 1),
  );
  $form['fieldset_node']['admin_links_d7_node_delete_tab_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable \'Delete\' form button on nodes.'),
    '#default_value' => variable_get('admin_links_d7_node_delete_tab_only', 1),
  );
  $form['fieldset_universaledit'] = array(
    '#type' => 'fieldset',
    '#title' => t('Universal Edit Button'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['fieldset_universaledit']['admin_links_d7_universaledit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add support for the <a href="@ueb">Universal Edit Button</a> on editable content.', array('@ueb' => 'http://universaleditbutton.org/')),
    '#default_value' => variable_get('admin_links_d7_universaledit', 0),
  );

  $form['#submit'][] = '_admin_links_d7_admin_settings_form_submit';
  return system_settings_form($form);
}

/**
 * Implements hook_form_submit().
 *
 * Submission handler for the system settings form.
 */
function _admin_links_d7_admin_settings_form_submit(&$form, &$form_state) {
  // When switching state for node deletion tabs we need to rebuild menu cache;
  // but this needs to happen after form_submit.
  // So we set a variable, and in the form itself we react on it.
  if (variable_get('admin_links_d7_node_delete_tab_active', 1) != $form_state['values']['admin_links_d7_node_delete_tab_active']) {
    variable_set('admin_links_d7_menu_rebuild_queued', 1);
  }
}
